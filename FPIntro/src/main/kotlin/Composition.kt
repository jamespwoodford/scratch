import arrow.core.compose
import arrow.syntax.function.curried

typealias Func<A, B> = (A) -> B

val getPrice: Func<Book, Price> = { book -> book.price }
val formatPrice: Func<Price, String> =
        fun(priceData: Price) = "value: ${priceData.value}${priceData.currency}"

//infix keyword notates that the function can be called using the infix notation (omitting the dot and parens)... infix has to be member functions or extension functions
//must have single param and param can't accept variable number of arguments or have default values
infix fun <A, B, C> Func<B, C>.after(f: Func<A, B>): Func<A, C  > = { x: A -> // defines a function after which is an extension to format price.  It invokes a function of type Func<B,C>
    this(f(x)) }  //after the function f (of type Func<A,B> is called.

val addThree = { a:Int, b: Int, c: Int ->
    a + b + c
}.curried()

fun main() {
    val result: String = formatPrice(getPrice(books[0]))
    println(result)


    val anotherResult: String = formatPrice.after(getPrice)(books[0]) // alternate way of writing
    println(anotherResult)

    val compositeResult: String = (formatPrice after getPrice)(books[0])
    println(compositeResult)

    // The equivalent of the "after" extension function we've building manually is provided in arrow as the
    // compose function.
    val compositeArrow: String = (formatPrice compose getPrice)(books[0])
    println(compositeArrow)

}
