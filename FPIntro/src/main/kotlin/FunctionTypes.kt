var anotherBook = Book(
        "0000000010",
        "Function Types Book",
        642,
        Price(50.00, "$"),
        3.0,
        2020,
        "Vincent Yee"
)

var anotherBookFun = fun(book: Book) = book.weight

typealias BookMapper<T> = (Book) -> T

fun main() {
    // mapper is a function of type BookMapper that refers to the bookWeight function in Functions.kt
    var mapper: BookMapper<Double> = ::bookWeight
    var currency: BookMapper<String> = { book -> book.price.currency}

    println("Weight of ${books[0].name} is ${mapper(books[0])} lbs")

    // assign mapper to refer to the bookPrice function from the Functions.kt file
    mapper = ::bookPrice

    println("Price of ${books[0].name} is ${mapper(books[0])}${currency(books[0])}")

}