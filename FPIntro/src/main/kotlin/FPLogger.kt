//the type Writer is a function that takes in a parameter of type T and creates a pair
typealias Writer<T, R> = (T) -> Pair<R, String>


// takes in a book and creates a Pair of the function getPrice(x) and a string.
val fpGetPrice: Writer<Book, Price> =
        fun(book: Book) = getPrice(book) to "Price calculated for ${book.ISDN}" //'to' creates a Pair<A,B> where its A to B

// takes in a price and creates a Pair of the function formatPrice(price) and a String
val fpFormatPrice: Writer<Price, String> =
        fun(price: Price) = formatPrice(price) to "Bill line created for ${formatPrice(price)}"


infix fun <A, B, C> Writer<A, B>.compose(f: Writer<B,C>): Writer<A, C> = {
        x: A ->
    val p1 = this(x) // p1 is the result of executing the Writer<A,B> function with the x parameter, resulting in a pair.
    val p2 = f(p1.first) // p2 is the result of executing the <B,C> function with the function in the pair resulting from
    println(p2.javaClass)
    p2.first to p1.second + "\n" + p2.second
}

fun main() {
    val getPriceWithLog = fpGetPrice compose fpFormatPrice
    books.forEach { book ->
        println(getPriceWithLog(book).second)
    }
}
