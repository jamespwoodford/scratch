import sun.invoke.empty.Empty

//Create an extension function to the List<Book> class which takes a function as a parameter of type BookMapper.
fun List<Book>.total(fn: BookMapper<Double>): Double =
        fold(0.0) {total, book -> total + fn(book)} //fold starts with the initial value and then uses the function provided to accumulating into total

var testReduce = listOf(1,2,3).reduce {sum, element -> sum + element} // reduce syntax

fun main() {
    val totalPrice = books.total { it.price.value } // 'it' keyword means we're referring to that objects values
    val totalWeight = books.total { it.weight }

    println("Total Price: $totalPrice $")
    println("Total Weight: $totalWeight")

    println("Test reduce: $testReduce")

    // for each is an extension function in the Kotlin standard lib
    books.forEach { println(it.name) }  // equivalent to writing books.forEach({println(it.name)} because when the last parameter is a lambda it can be written outside
}

